import boto3
import requests
import sys
sys.path.append("")
import pandas as pd
from gtfsrttrupdates.gtfs_static.scrape_static_feed import scrape_static_gtfs_s3
from gtfsrttrupdates.gtfs_rt.scrape_trip_updates import scrape_trip_updates_s3
from gtfsrttrupdates.gtfs_filter.filter_gtfs_main import filter_enrich_gtfs
from utils.settings import key_ID_s3, key_pass_s3


def main(s3):
    """Function that executes
    GTFS static once a day
    GTFS realtime every minute
    GTFS filter every minute

    Parameters
    ----------
    s3: s3 object, required

    Returns
    ------
    Statuscode

    """
    try:
        # scrape gtfs zipfile naar s3 (incl uitpakken)
        flag_file_exists = scrape_static_gtfs_s3(s3)

        # update trip update
        trip_updates = scrape_trip_updates_s3(s3)

        # filter gtfs and trip updates - store on s3 (die nu nog checken)
     #   filter_enrich_gtfs(s3, trip_updates=trip_updates, file_exists=flag_file_exists)

    except requests.RequestException as e:
        print(e)
        raise e

    return {
        "statusCode": 200,
        "body": 'done'
    }


if __name__ == "__main__":
    s3_resource = boto3.resource('s3', aws_access_key_id=key_ID_s3, aws_secret_access_key=key_pass_s3)

    result = main(s3=s3_resource)

    print(result)
