import pandas as pd
import requests
from io import StringIO, BytesIO
import os
import botocore
from datetime import datetime
from gtfsrttrupdates.gtfs_rt import gtfs_realtime_pb2
from gtfsrttrupdates.utils.settings import gtfs_files, bucket, folder_static

def get_feed(url):
	response = requests.get(url)
	feed = gtfs_realtime_pb2.FeedMessage()
	feed.ParseFromString(response.content)
	return feed


def tu_feed_change(feed):
	feed_new = get_tripupdates()

	if feed == None:
		return True
	
	if feed_new.entity != feed.entity:
		return True
	else:
		return False

def get_positions(url='http://gtfs.ovapi.nl/nl/vehiclePositions.pb'):
	return get_feed(url)


def get_tripupdates(url='http://gtfs.ovapi.nl/nl/tripUpdates.pb'):
	return get_feed(url)




def tripupdates_to_df(feed, route_id=None):
	ids = []
	trip_ids = []
	start_times = []
	start_dates = []
	schedule_relationships = []
	route_ids = []
	vehicle_ids = []
	stop_sequences = []
	arrival_delays = []
	arrival_times = []
	departure_delays = []
	departure_times = []
	stop_ids = []
	labels = []
	
	for entity in feed.entity:
		if entity.trip_update.trip.route_id == str(route_id) or route_id==None:
			stop_time_updates = entity.trip_update.stop_time_update
			
			
			for stop_time_update in stop_time_updates:
				ids.append(entity.id)
				trip_ids.append(entity.trip_update.trip.trip_id)
				start_times.append(entity.trip_update.trip.start_time)
				start_dates.append(entity.trip_update.trip.start_date)
				schedule_relationships.append(entity.trip_update.trip.schedule_relationship)
				route_ids.append(entity.trip_update.trip.route_id)
				vehicle_ids.append(entity.trip_update.vehicle.id)
				stop_sequences.append(stop_time_update.stop_sequence)
				
				if stop_time_update.arrival:
					arrival_delays.append(stop_time_update.arrival.delay)
					arrival_times.append(stop_time_update.arrival.time)
				else:
					arrival_delays.append(None)
					arrival_times.append(None)

				if stop_time_update.departure:					
					departure_delays.append(stop_time_update.departure.delay)
					departure_times.append(stop_time_update.departure.time)
				else:
					departure_delays.append(None)
					departure_times.append(None)


				stop_ids.append(stop_time_update.stop_id)

				if entity.trip_update.vehicle:
					labels.append(entity.trip_update.vehicle.label)
				else:
					labels.append(None)

	df = pd.DataFrame()
	
	df['id'] = ids
	df['trip_id'] = trip_ids
	df['start_time'] = start_times
	df['start_date'] = start_dates
	df['schedule_relationship'] = schedule_relationships
	df['route_id'] = route_ids
	df['vehicle_id'] = vehicle_ids
	df['stop_sequence'] = stop_sequences
	df['arrival_delay'] = arrival_delays
	df['arrival_time'] = arrival_times
	df['departure_delay'] = departure_delays
	df['departure_time'] = departure_times
	df['stop_id'] = stop_ids
	df['label_id'] = labels
	df['dataset_timestamp'] = str(feed.header.timestamp)

	'''
	CHANGED INT TYPE TO STRING BECAUSE OF OVERFLOWERROR
	'''
	df['trip_id'] = df.trip_id #.astype(float)
	df['stop_id'] = df.stop_id #.astype(float)

	return df


def load_csv_as_df_from_s3(s3, bucket, location, filename, filter_load=False, filter_list=None):
	# compose key
	key = os.path.join(location, filename)
	print('Loading csv as df from bucket: '+bucket+', key: '+key)
	# get object connection

	try:
		# get object from bucket
		# obj = s3.Object(bucket, key).get()
		obj = s3.Object(bucket, key).get()['Body']
	except requests.RequestException as e:
		print(e)
		raise e

	print('loading object from s3. filtering set to: ' + str(filter_load))

	if filter_load:
		print('filtering a total of ' + str(len(filter_list)) + ' values')
		# iterate through object and filter on trip id's (can filter stop_times and trip_updates
		iter_csv = pd.read_csv(obj, iterator=True, chunksize=400000, dtype=str)
		chunks = []
		for chunk in iter_csv:
			chunks.append(chunk[chunk['trip_id'].isin(filter_list)])
			print(str(len(chunks)))
		df = pd.concat(chunks)
	else:
		df = pd.read_csv(obj, dtype=str)
	print('returning df')
	return df


def store_df_as_csv_on_s3(s3, bucket, location, filename, df):
	# compose key
	key = os.path.join(location, filename)

	# open buffer
	csv_buffer = StringIO()
	df.to_csv(csv_buffer, index=False)

	# put result in bucket
	s3.Object(bucket, key).put(Body=csv_buffer.getvalue())


def bucket_file_exists(s3, bucket, filepath):
	print('checking if '+ os.path.join(bucket,filepath) + ' exists')
	try:
		# load() does a HEAD request for a single key, which is fast
		s3.Object(bucket, filepath).load()
		print('-- file exists!')
		return True
	except botocore.exceptions.ClientError as e:
		if e.response['Error']['Code'] == "404":
			# object bestaat niet
			return False
		else:
			print("unknown error - paniek")
			raise

	else:
		return True


def load_gtfs_files_s3(s3, date, bucket=bucket, folder=folder_static, load_stop_times=False, trip_filter=None):
	"""
    #load files. kiezen - of statid except stop_times, of alleen stop_times met filter
    :param s3:
    :param date:
    :param bucket:
    :param folder:
    :param load_stop_times:
    :param trip_filter:
    :return:
    """
	# empty dict to store output
	dict_output = {}
	# where to load files from?
	location = os.path.join(folder, date)

	# select which files to load
	if load_stop_times:
		file = gtfs_files[-1]

		key_name = str(file)
		print('loading ' + key_name)
		filename = file + '.txt'
		dict_output[key_name] = load_csv_as_df_from_s3(s3, bucket, location, filename, filter_load=load_stop_times,
													   filter_list=trip_filter)
	else:
		filelist = gtfs_files[:-1]

		# loop through files and store in dictionary
		for file in filelist:
			key_name = str(file)
			print('loading '+ key_name)
			filename = file + '.txt'
			dict_output[key_name] = load_csv_as_df_from_s3(s3, bucket, location, filename, filter_load=load_stop_times,
														   filter_list=trip_filter)

	print('finished loading files')
	return dict_output


def load_filter_trip_updates_s3(s3, bucket, folder, date, trip_filter):
	"""
    #load files. kiezen - of statid except stop_times, of alleen stop_times met filter
    :param s3:
    :param bucket:
    :param folder:
    :param date:
    :param trip_filter:
    :return:
    """

	# where to load files from?
	location = os.path.join(folder, date)

	# filename from supplied date (change format)
	date_format = datetime.strptime(date, '%Y%m%d').strftime('%Y-%m-%d')
	#filename = r'trip_updates_' + date_format + r'_full.csv'
	filename = 'trip_updates_{date}_full.csv'.format(date=date_format)
	#filename = f'trip_updates_{date_format}_full.csv'

	# loop through files and store in dictionary
	trip_updates = load_csv_as_df_from_s3(s3, bucket, location, filename, filter_load=True, filter_list=trip_filter)

	try:
		column_list = ["trip_id", "stop_id", "stop_sequence", "arrival_delay", "departure_delay", "dataset_timestamp"]
	except:
		column_list = ["trip_id", "stop_id", "stop_sequence", "arrival_delay", "departure_delay"]
	trip_updates = trip_updates[column_list]

	return trip_updates





