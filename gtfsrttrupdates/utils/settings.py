import time
import pytz
from pytz import timezone
from datetime import datetime

#  other settings
gtfs_files = ["agency", "calendar_dates", "feed_info", "routes", "stops", "transfers", "trips", "shapes", "stop_times"]
#agency_list = ["HTM", "HTMBUZZ", "RET"]

# use timezone
timezone = timezone('Europe/Amsterdam')

# deze moet dynamisch
filter_date = datetime.now(tz=timezone).strftime("%Y-%m-%d")

# S3 settings
bucket = 'amelie-mand'
key_ID_s3 = 'AKIA2VUQJLJ4RDVS4J4V'
key_pass_s3 = 'U0K9vtOS3Nh972mJ+g4GAfdfYVtRKLkMd34XAnii'

# GTFS_STATIC settings
# For SL, using the API key of Sander
source_static = 'http://gtfs-pp.samtrafiken.se/sl/sl.zip?key=d1dd3f16-1d51-4829-b1e4-81b449d0c753'
#source_static = 'http://gtfs.ovapi.nl/nl/gtfs-nl.zip'
folder_static = 'gtfs_static_sl'

# GTFS_RT settings
folder_rt = 'gtfs_rt_sl'
source_rt = 'https://gtfsr-pp.samtrafiken.se/sl/TripUpdates.pb?key=d1dd3f16-1d51-4829-b1e4-81b449d0c753'

# setting for the filter folder
folder_filter = 'gtfs_filter_sl'


