from utils.utils import *
from utils.settings import folder_rt, bucket, filter_date, source_rt
import os


def update(df_old, df_new):
	df = df_old.append(df_new, sort=False)
	df = df.drop_duplicates(subset=['id', 'trip_id', 'stop_id', 'start_time'], keep='last')
	#df = df.groupby(['id', 'trip_id', 'stop_id', 'start_time'], as_index=False).last()
	return df


# scrape trip updates - update eventueel bestaande file met behulp van update function
def scrape_trip_updates_s3(s3, bucket=bucket, folder=folder_rt, date=filter_date):

	#bij wissel dag wordt hier nieuwe date variable gemaakt -> en nieuwe file voor het eerst
	filename = 'trip_updates_{date}_full_incl_vehicleid.csv'.format(date=date)
	#filename = f'trip_updates_{date}_full.csv'

	# create full filepath incl. filename, and file location
	location = os.path.join(folder, date)
	key = os.path.join(folder, date, filename)
	print(key)

	feed = get_tripupdates(url=source_rt)
	print("got trip updates")

	file_exists = bucket_file_exists(s3, bucket, key)

	# check if today's folder exists. if not, run without update function
	if not file_exists:
		# load, write csv
		print("no file found, writing to new folder")
		df = tripupdates_to_df(feed)

		# store on s3
		store_df_as_csv_on_s3(s3, bucket, location, filename, df)
		print("stored file on s3")
	else:
		# load, update and write csv
		print("file found, updating")

		# get old file
		df = load_csv_as_df_from_s3(s3, bucket, location, filename)

		# update file
		df = update(df, tripupdates_to_df(feed))

		# store on s3
		store_df_as_csv_on_s3(s3, bucket, location, filename, df)
		print("updated file on s3")

	return df

