from datetime import datetime
import os
from utils.utils import load_gtfs_files_s3, load_csv_as_df_from_s3
# from utils.settings import agency_list, filter_date, bucket, folder_rt
from utils.settings import filter_date, bucket, folder_rt



def filter_gtfs_on_date(s3, date):
    """
    load all gtfs files and filter on given date

    :param s3:
    :param date:
    :return:
    """

    # change date format for loading
    date_format = datetime.strptime(date, '%Y-%m-%d').strftime('%Y%m%d')
    print('filtering gtfs on date')
    # load dict with data from files on s3
    gtfs_files = load_gtfs_files_s3(s3, date)

    print('all gtfs files except stop times loaded')

    # transform dict into objects for easier processing
    # Let op - geen stop times - die kunnen we pas laden nadat we weten welke trip id's te filteren
    agency = gtfs_files["agency"]
    calendar_dates = gtfs_files["calendar_dates"]
    feed_info = gtfs_files["feed_info"]
    routes = gtfs_files["routes"]
    shapes = gtfs_files["shapes"]
    trips = gtfs_files["trips"]
    stops = gtfs_files["stops"]
    transfers = gtfs_files["transfers"]

    print('\n.. filtering the gtfs files on date')
    # filter on date
    calendar_dates = calendar_dates[calendar_dates['date'].astype(str) == date_format]
    trips = trips[trips['service_id'].isin(calendar_dates['service_id'])]
    shapes = shapes[shapes['shape_id'].isin(trips['shape_id'])]
    routes = routes[routes['route_id'].isin(trips['route_id'])]

    # stop times is moeilijker want moeten tijdens het laden al filteren
    # stop times moet incrementeel! niet genoeg geheugen op lambda om dit zonder chunks te doen

    # list of trip id's to be extracted from stop times
    trip_id_list = trips['trip_id'].unique()

    # load and filter stop times from s3 with trip id filter
    stop_times = load_gtfs_files_s3(s3, date, load_stop_times=True, trip_filter=trip_id_list)["stop_times"]

    stops = stops[stops['stop_id'].isin(stop_times['stop_id'])]
    agency = agency[agency['agency_id'].isin(routes['agency_id'])]
    transfers = transfers[
        transfers['from_stop_id'].isin(stops['stop_id']) &
        transfers['to_stop_id'].isin(stops['stop_id']) &
        transfers['from_trip_id'].isin(trips['trip_id']) &
        transfers['to_trip_id'].isin(trips['trip_id'])]

    print('\n.. filtering on date done')

    # return the files (no trip_updates)
    return agency, calendar_dates, feed_info, routes, stop_times, stops, transfers, trips, shapes


def filter_gtfs_on_date_and_agency(s3, date, agency_filter):
    """
    load gtfs files of a date and filter on given agencies

    :param s3:
    :param date:
    :param agency_filter:
    :return:
    """

    # load the files (filtered on date)
    agency, calendar_dates, feed_info, routes, stop_times, stops, transfers, trips, shapes = filter_gtfs_on_date(s3, date)

    # create agency filtered gtfs files
    print('\n.. filtering the gtfs files on agency')
    '''
    FOR SL: do not filter on agency for now
    '''

  #  agency = agency[(agency['agency_id'].isin(agency_filter))]
    routes = routes[routes['agency_id'].isin(agency['agency_id'])]
    trips = trips[trips['route_id'].isin(routes['route_id'])]
    shapes = shapes[shapes['shape_id'].isin(trips['shape_id'])]
    calendar_dates = calendar_dates[calendar_dates['service_id'].isin(trips['service_id'])]
    stop_times = stop_times[stop_times['trip_id'].isin(trips['trip_id'])]
    stops = stops[stops['stop_id'].isin(stop_times['stop_id'])]
    transfers = transfers[
        transfers['from_stop_id'].isin(stops['stop_id']) &
        transfers['to_stop_id'].isin(stops['stop_id']) &
        transfers['from_trip_id'].isin(trips['trip_id']) &
        transfers['to_trip_id'].isin(trips['trip_id'])]

    # return the files (no trip_updates)
    return agency, calendar_dates, feed_info, routes, stop_times, stops, transfers, trips, shapes


def filter_trip_updates(trip_updates, trip_filter):
    """
    load gtfs trip updates of a date and filter on given agencies (through trip id's)

    :param trip_updates:
    :param trip_filter:
    :return:
    """
    # change date format for loading
    print('filtering trip updates')

    trip_updates_filtered = trip_updates[trip_updates['trip_id'].isin(trip_filter)]

    column_list = ["trip_id", "stop_id", "stop_sequence", "arrival_delay", "departure_delay"]
    trip_updates_filtered = trip_updates_filtered[column_list]

    print('trip updates filtered')

    return trip_updates_filtered


# def load_filter_trip_updates(s3, date, trip_filter, bucket=bucket, folder=folder_rt):
#     """
#     load gtfs trip updates of a date and filter on given agencies (through trip id's)
#
#     :param s3:
#     :param date:
#     :param trip_filter:
#     :param bucket:
#     :param folder:
#     :return:
#     """
#     # change date format for loading
#     print('loading/filtering trip updates')
#
#     date_format = datetime.strptime(date, '%Y%m%d').strftime('%Y-%m-%d')
#     location = os.path.join(folder, date_format)
#     filename = f'trip_updates_{date_format}_full.csv'
#
#     # load trip updates from s3
#     trip_updates = load_csv_as_df_from_s3(s3, bucket, location, filename, filter_load=True, filter_list=trip_filter)
#
#     # keep only relevant columns from trip_updates
#     column_list = ["trip_id", "stop_id", "stop_sequence", "arrival_delay", "departure_delay"]
#     trip_updates = trip_updates[column_list]
#
#     print('trip updates filtered')
#
#     return trip_updates
