import pandas as pd
import numpy as np
import os
from datetime import datetime
from utils.utils import load_csv_as_df_from_s3, store_df_as_csv_on_s3, bucket_file_exists
from utils.settings import  filter_date, bucket, folder_filter
from gtfs_filter.filter_gtfs_on_date import filter_gtfs_on_date_and_agency, filter_trip_updates


# function to fill delays not defined in trip updates
def join_fill_delays(stop_times):
    """
    alleen aanpassingen in de delays worden geleverd
    als we van stop seq. 1 en 4 iets weten, moet 2 en 3 gelijk zijn aan 1

    :param stop_times:
    :return:
    """

    print('\n.. filling empty trip_updates info using ffill')

    #  sort values
    stop_times["stop_sequence"] = pd.to_numeric(stop_times["stop_sequence"])
    stop_times.sort_values(by=["trip_id", "stop_sequence"], ascending=[True, True], inplace=True)

    #  fill NA values in columns arrival_delay/departure_delay. stop steeds einde rit. Huidige rit wordt wel gevuld
    stop_times["arrival_delay"] = stop_times.groupby("trip_id").arrival_delay.transform(lambda x: x.ffill())
    stop_times["departure_delay"] = stop_times.groupby("trip_id").departure_delay.transform(lambda x: x.ffill())

    print('\n.. done filling')

    return stop_times


# zou nog slimmer moeten kunnen - dan moeten we weten welke gerealiseerde trips al zijn gejoined
def join_trip_updates_to_static(stop_times, trip_updates):
    """
    Hier voegen we de gerealiseerde info to aan de stop_times

    :param stop_times:
    :param trip_updates:
    :return:
    """

    stop_times[['trip_id', 'stop_id', 'stop_sequence']] = stop_times[['trip_id',
                                                                      'stop_id',
                                                                      'stop_sequence']].apply(pd.to_numeric)
    trip_updates[['trip_id', 'stop_id', 'stop_sequence']] = trip_updates[['trip_id',
                                                                          'stop_id',
                                                                          'stop_sequence']].apply(pd.to_numeric)

    print('\n.. joining trip_updates info to stop_times file')
    stop_times_enriched = pd.merge(left=stop_times,
                                   right=trip_updates,
                                   how='left',
                                   on=['trip_id', 'stop_id', 'stop_sequence'])

    # stop_times_enriched = stop_times.merge(trip_updates, how='left') # merge op trip_id, stop_id en stop_sequence

    print('\n.. info joined')

    #  fill NA's
    stop_times_filled = join_fill_delays(stop_times_enriched)

    return stop_times_filled


# hier gebeurt het allemaal. Elke dag 1x alles, daarna alleen nieuwe stop_times_enriched file (obv van flag)
def filter_enrich_gtfs(s3, trip_updates, bucket=bucket, date=filter_date, file_exists=False):
    """
    # filter gtfs on agency/date, create enriched stop_times.txt, store in gtfs_filter
    :param s3:
    :param trip_updates:
    :param bucket:
    :param date:
    :param file_exists:
    :return:
    """
    # relevant location and date
    location = os.path.join(folder_filter, date)
    location_today = os.path.join(folder_filter, 'today')

    # file exists flag komt uit ophalen static data. Als eerste run dan false, anders true
    if file_exists and bucket_file_exists(s3, bucket, os.path.join(location_today, 'agency.txt')):

        # load trips and stop_times - neccessary to get updated stop_times_enriched file
        trips = load_csv_as_df_from_s3(s3, bucket, location_today, 'trips.txt')
        stop_times = load_csv_as_df_from_s3(s3, bucket, location_today, 'stop_times_enriched.txt')

        # remove existing trip updates column to be able to join update
        stop_times.drop(columns=['arrival_delay', 'departure_delay'], inplace=True)

        # Load and filter trip updates (using relevant trip id's from filtered static)
        trip_id_list = trips['trip_id'].unique()
        filtered_trip_updates = filter_trip_updates(trip_updates, trip_id_list)

        # enrich stop times with trip_updates (gerealiseerde dienstregeling)
        stop_times_enriched = join_trip_updates_to_static(stop_times=stop_times,
                                                          trip_updates=filtered_trip_updates)

        # store enriched stop time file
        # store_df_as_csv_on_s3(s3, bucket, location, 'stop_times_enriched.txt', stop_times_enriched)
        store_df_as_csv_on_s3(s3, bucket, location_today, 'stop_times_enriched.txt', stop_times_enriched)

    else:
        # load and filter the static gtfs files
        agency, calendar_dates, feed_info, routes, stop_times, stops, transfers, trips, shapes = \
            filter_gtfs_on_date_and_agency(s3, date=filter_date, agency_filter=None)

        # filter trip updates (using relevant trip id's from filtered static)
        print('loaded local files')
        trip_id_list = trips['trip_id'].unique()
        filtered_trip_updates = filter_trip_updates(trip_updates, trip_id_list)

        # enrich stop times with trip_updates (gerealiseerde dienstregeling)
        stop_times_enriched = join_trip_updates_to_static(stop_times=stop_times,
                                                          trip_updates=filtered_trip_updates)

        # store filtered files
        store_df_as_csv_on_s3(s3, bucket, location_today, 'agency.txt', agency)
        store_df_as_csv_on_s3(s3, bucket, location_today, 'calendar_dates.txt', calendar_dates)
        store_df_as_csv_on_s3(s3, bucket, location_today, 'feed_info.txt', feed_info)
        store_df_as_csv_on_s3(s3, bucket, location_today, 'routes.txt', routes)
        store_df_as_csv_on_s3(s3, bucket, location_today, 'shapes.txt', shapes)
        store_df_as_csv_on_s3(s3, bucket, location_today, 'stops.txt', stops)
        store_df_as_csv_on_s3(s3, bucket, location_today, 'transfers.txt', transfers)
        store_df_as_csv_on_s3(s3, bucket, location_today, 'trips.txt', trips)

        # store enriched stop time file
        # store_df_as_csv_on_s3(s3, bucket, location, 'stop_times_enriched.txt', stop_times_enriched)
        store_df_as_csv_on_s3(s3, bucket, location_today, 'stop_times_enriched.txt', stop_times_enriched)
