import requests
import boto3
import io
import os
from datetime import datetime
from zipfile import ZipFile
from utils.utils import bucket_file_exists
from utils.settings import source_static, folder_static, bucket, filter_date


# schrijft unzipped gtfs static files naar map op s3.
# max gebruikte memory is 360MB
def scrape_static_gtfs_s3(s3, bucket=bucket, folder=folder_static, url=source_static, date=filter_date):

    location = os.path.join(folder, date)
    # check if gtfs file already exists. if not, get/unzip/store
    if not bucket_file_exists(s3, bucket, os.path.join(location,'agency.txt')):
        print('scraping static files')
        # load zip file from gtfs api
        req = requests.get(url)
        # load into buffer using zipfile
        z = ZipFile(io.BytesIO(req.content))

        # store each file in zip to s3 in correct folder
        for filename in z.namelist():
            print(filename)
            put_key = os.path.join(location, filename)
            s3.meta.client.upload_fileobj(
                z.open(filename),
                Bucket=bucket,
                Key=put_key
            )
        print('stored unzipped static files to s3')
        return False
    else:
        # files already exist
        print('static files exist on s3')
        return True




